# Breve descrição

Cifra de Feistel balanceada, com blocos de 64 bits e chave de 64 bits.
A função interna não é invertível, e usa chaves de rodada de 32 bits.

# S-boxes e funções de substituição

`S4_1`, `S4_2` sào invertíveis. `S4_3`, `S4_4` não são.

```
S4_1:
0x8, 0xA, 0xB, 0x1, 0xF, 0x4, 0x7, 0xE, 0x5, 0xD, 0x0, 0x2, 0x3, 0x6, 0xC, 0x9 

S4_2:
0xE, 0x7, 0x4, 0xF, 0x1, 0xB, 0xA, 0x8, 0x9, 0xC, 0x6, 0x3, 0x2, 0x0, 0xD, 0x5

S4_3:
0x2, 0xE, 0x1, 0x5, 0x9, 0xA, 0x4, 0x8, 0x8, 0x0, 0xD, 0x7, 0xC, 0x3, 0xF, 0x6

S4_4:
0xA, 0x5, 0xD, 0x2, 0x0, 0xF, 0x1, 0x7, 0xD, 0x6, 0x9, 0xE, 0x3, 0xB, 0x4, 0xC
```

# Estrutura da cifra

É uma rede de Feistel.

Entrada da rodada: `L || R`

Saída: `L' || R'`, onde `L' = R`, e `R' =  L + f(R)`, onde `f()` é a função de rodada.

# Função de rodada f()

1. Rotacione 10 bits à esquerda ( `X' = X << 10` ).
2. Quebre o resultado `X'` em quatro bytes, `ABCD`
3. Seja `X''` igual à concatenação \
`S4_14(A) || S4_23(B) || S4_42(C) || S4_31(D)` 
4. Aplique (ou-exclusivo) a i-ésima chave de rodada.

# P-box

Uma permutação é aplicada após cada rodada. Os 64 bits são divididos em oito bytes,
e o j-ésimo bit do i-ésimo byte passa a ser o i-ésimo bit to j-ésimo byte.
Por exemplo, no QUARTO byte da entrada, o TERCEIRO bit:
será enviado para o QUARTO **BIT** do TERCEIRO **BYTE** da
saída.

\newpage 

```       
               3                  4                  5         ... <= bytes
                          +----------------+
                          | . . . B . . . .|
                          +----------------+
                                  |
                   ______________/
                  /
                 |
                 v
       +----------------+
       | . . . . B . . .|
       +----------------+
```

# Escalonamento de chaves

Uma chave de 64 bits é determinada inicialmente.

A função de escalonamento aceita explicitamente o número da rodada (`round`).

1. Rotacione a chave mestra `round * 5` para a esquerda.
2. Divida o resultado em oito bytes, `ABCDEFGH`
3. Em rodadas ímpares, retorne \
`S4_14(Ak) || S4_23(Bk) || S4_42(Ck) || S4_31(Dk)`
4. Em rodadas pares, retorne \
`S4_14(Hk) || S4_23(Gk) || S4_42(Fk) || S4_31(Ek)`
