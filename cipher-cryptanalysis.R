# cipher-cryptanalysis.R


# Copyright (C) 2015 Jerônimo C. Pellegrini
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to:
# The Free Software Foundation, Inc.,
# 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA



# Exemplo de criptanálise linear e diferencial, usando a cifra de
# brinquedo das notas de aula em http://aleph0.info/cursos/ic
# (o Capítulo sobre Criptanálise deve esclarecer muito sobre este
# programa)
#
# Uso:
#
# R cipher-cryptanalysis.R --no-save
#
# Isto gerará dois arquivos,
#
# count.key: contém as chaves geradas aleatoreamente, e indica a 
# subchave candidata que será buscada.
#
# count.dat: lista as possíveis chaves candidatas, com suas  probabilidades
# (baseando-se nas frequências obtidas).



library(compiler)
#enableJIT(3)
library(bitops)

# Dado número, retorna uma string com sua representação
# em binário:
asbin <- function (n) {
   paste(sapply(strsplit(paste(rev(intToBits(n))),""),`[[`,2),collapse="")
}

# Dada uma string com um número binário, retorna o inteiro
# correspondente.
parsebin <- function (s) {
  total <- 0
  size <- nchar(s)
  for (pos in 1:size)
    if (substr(s,size-pos+1,size-pos+1)=='1')
      total <- total + (2^(pos-1))
  total
}
  
# S-box (para 4 bits) e inversa
#
# os números aqui são a saída. A entrada é 0,1,...,F.
sbox     <- c( 2,     8, 0xD, 5, 4, 0xA, 0xC, 0xF, 0xE,   3, 0xB,   9, 7, 1, 6, 0 )
inv_sbox <- c( 0xF, 0xD,   0, 9, 4,   3, 0xE, 0xC,   1, 0xB,   5, 0xA, 6, 2, 8, 7 )

# Permutação (para 16 bits)
pbox <- c(1,5,9,13,2,6,10,14,3,7,11,15,4,8,12,16)

# Dada sequência de 16 bits, *REPRESENTADA COMO INTEIRO*,
# dividimos em quatro partes e aplicamos a S-box em cada parte
sub <- function (i) {
  one   <- bitAnd(bitShiftR(i,12),15)
  two   <- bitAnd(bitShiftR(i,8),15)
  three <- bitAnd(bitShiftR(i,4),15)
  four  <- bitAnd(i,15)
  left  <- bitOr (bitShiftL(sbox[1+one],  4), sbox[1+two])
  right <- bitOr (bitShiftL(sbox[1+three],4), sbox[1+four])
  out   <- bitOr (bitShiftL(left,8),right)
  out
}

# Dada sequência de 16 bits, *REPRESENTADA COMO INTEIRO* x,
# mostra na tela em binário a substituição aplicada a x, e retorna também
# uma string mostrando a representação hexa de x e sub(x)
show_sub <- function(x) {
  cat (x, "<-->", as.hexmode(x), "\n")
  print (substring(asbin(x),17))
  print (substring(asbin(sub(x)),17))
  cat (sub(x), "<-->", as.hexmode(sub(x)),"\n")
}

# Inversa de sub: Dada sequência de 16 bits, *REPRESENTADA COMO INTEIRO* x,
# retorna outro inteiro, com  a inversa de sub aplicada em x.
inv_sub <- function (i) {
  one   <- bitAnd(bitShiftR(i,12),15)
  two   <- bitAnd(bitShiftR(i,8),15)
  three <- bitAnd(bitShiftR(i,4),15)
  four  <- bitAnd(i,15)
  left  <- bitOr (bitShiftL(inv_sbox[1+one],  4), inv_sbox[1+two])
  right <- bitOr (bitShiftL(inv_sbox[1+three],4), inv_sbox[1+four])
  out   <- bitOr (bitShiftL(left,8),right)
  out
}

# Lê o i-ésimo bit de um número
bitget <- function (x,b) {
  bitAnd(bitShiftR(x,b-1),1)
}

# Muda o i-ésimo bit de um número opara um. Não modifica x;
# ao invés disso, retorna um novo inteiro
bitset <- function (x,pos) {
  bitOr(bitShiftL(1,pos-1),x)
}

# Dada sequência de 16 bits, *REPRESENTADA COMO INTEIRO*,
# realiza a permutação nele. Não modifica x;
# ao invés disso, retorna um novo inteiro
perm <- function  (x) {
  o <- 0;
  for(bit in 1:16) {
    if ( 1 == bitget(x,17 - bit) ) {
      o <- bitset(o,  pbox[17-bit])
    }
  }
  o;
}

# Recebe sequência de 16 bits, *REPRESENTADA COMO INTEIRO*, e
# mostra na tela, em binário, a permutação imposta por perm(.).
# Não modifica x
show_perm <- function(x) {
  print (x)
  print (substring(asbin(x),17))
  print (substring(asbin(perm(x)),17))
  print (perm(x))
}


# Uma rodada da cifra
netround <- function (x,key) {
  perm(sub(bitXor(x,key)))
}


# Encriptação (todas as rodadas)
crypt <- function (m,k) {
  c <- netround(m,k[1])
  c <- netround(c,k[2])
  c <- netround(c,k[3])
  c <- sub(bitXor(c, k[4]))
  bitXor(c, k[5])
}


#
# Criptanálise
#

# Gera n mensagens aletórias de 16 bits
gen_messages <- function (n) {
  sample(1:2^16-1,n,replace=F)
}

# Encripta todas as mensagens no vetor m com
# a chave k.
enc_messages <- function (m,k) {
  vapply(m,function (msg) {crypt(msg,k)}, 1)
}


##
## Linear:
##

# count: coração da criptanálise linear; conta a frequência
#  com que a relação linear vale, para cada subchave e para todos
# os pares m/c
#
count <-  function (m,c) {
  # freq é vetor com 256 posições (temos 2^8 bits na chave candidata,
  # logo 2^8=256 posições para contar cada ocorrência de padrão
  freq <- rep(0,times=2^8)

  for (key_idx in 1:(2^8)) {
    key <- bitShiftL((key_idx-1),4);
    cat ("Key#", key_idx, " (", key, ")\n");
    for (i in 1:length(m)) {
      o <- bitXor(c[i],key);
      x <- inv_sub(o);
      if (0 == bitXor( bitXor( bitXor (bitget(x, 17-6),
                                       bitget(x, 17-8)),
                               bitXor (bitget(x, 17-10),
                                       bitget(x, 17-12))),
                       bitXor (bitget(m[i], 17-1),
                               bitget(m[i], 17-2)))) {
        freq[key_idx] <- freq[key_idx] + 1;
      }
    }
  }
  freq;
}

# Compilamos a função count:
ccount <- cmpfun(count)


# Esta é a função que realmente faz a criptanálise linear.
linear_cryptanalysis <- function() {

  # chave <- sequência aleatórea de 16 bits.
  # usamos 5 repetições, porque temos uma para cada rodada
  k  <- sample(1:2^16-1,5,replace=T)

  # gera e encripta 4000 mensagens com a chave
  n  <- 4000;
  ms <- gen_messages(n);
  cs <- enc_messages(ms,k);

  # conta. f será um vetor com 256 posições, contendo 
  # a frequência de cada padrão.
  f  <- ccount(ms,cs);
  
  output_key <- file("count.key","w")
  for (key in 1:5)
    cat("key: ", asbin(k[key]),"\n", file=output_key);
  target <- 2^8-1;
  target <- bitShiftL(target,4);
  target <- bitAnd(target,k[5]); # somente k[5] é quem nos interessa
  target <- bitShiftR(target,4);
  cat ("target subkey: ", substring(asbin(target),17), "\n",file=output_key);
  
  output <- file("count.dat","w");
  for (key in 1:(2^8)) 
    cat(substring(asbin(key-1),17),
       signif(abs(f[key]-n/2)/n,4),
       "\n",
       file=output);
}

###
linear_cryptanalysis()
###



##
## Diferencial
##

# Gera as mensagens encriptadas
gen_messages_2 <- function (m,delta_x) {
    vapply(m, function (msg) {bitXor(msg,delta_x)}, 1)
}


# Realiza a contagem
diff_count <-  function (c1,c2,delta_y) {
  freq <- rep(0,times=2^8)
  for (key_idx in 1:(2^8)) {
    key <- bitShiftL((key_idx-1),8);
    cat ("Key#", key_idx, " (", key, ")\n");
    for (i in 1:length(c1)) {
      o1 <- bitXor(c1[i],key);
      o2 <- bitXor(c2[i],key);
      x1 <- inv_sub(o1);
      x2 <- inv_sub(o2);
      if (bitXor(x1,x2) == delta_y)
        freq[key_idx] <- freq[key_idx] + 1;
    }
  }
  freq;
}


# Aqui a criptanálise diferencial é de fato realizada
diff_cryptanalysis <- function() {
  delta_x <- 0x0006;
  delta_y <- parsebin("0110011000000000")
  
  n  <- 800;
                                        #  k  <- sample(1:2^16-1,5,replace=T)
  sk <- c("00000000000000000100100010010101",
          "00000000000000001101000000010110",
          "00000000000000001101000001101111",
          "00000000000000001001011011100100",
          "00000000000000000101101111101101");
  
  k <- vapply(sk,parsebin,1)

  # generate a set m1 of messages, and m2 in which each
  # msg is *exactly*  delta_x different from another in m1
  m1 <- gen_messages(n);
  m2 <- gen_messages_2(m1,delta_x);

  # encrypt both sets
  c1 <- enc_messages(m1,k);
  c2 <- enc_messages(m2,k);

  # count frequencies of delta_y
  f  <- diff_count(c1,c2,delta_y);
  
  output_key <- file("count.key","w");
  for (key in 1:5) 
    cat("key: ", asbin(k[key]),"\n", file=output_key);
  target <- 2^8-1;
  target <- bitShiftL(target,8);
  target <- bitAnd(target,k[5]);
  target <- bitShiftR(target,8);
  cat ("target subkey: ", substring(asbin(target),17), "\n",file=output_key);
  
  output <- file("count.dat","w");
  for (key in 1:(2^8))
    cat(substring(asbin(key-1),17),
        signif(f[key]/n,4),
        "\n",
        file=output);
}


# Descomente para fazer a criptanálise diferencial
###
#diff_cryptanalysis()
###
