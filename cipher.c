/*
 *  cipher.c  -- Implementação didática de cifra de Feistel
 *
 *  Copyright © 2023 Jeronimo Pellegrini - <jeronimo.pellegrini@ufabc.edu.br>
 *
 *  PORTUGUÊS:
 *
 *  Este programa é um software livre: você pode redistribuí-lo e/ou
 *  modificá-lo sob os termos da Licença Pública Geral GNU, conforme
 *  publicado pela Free Software Foundation, seja a versão 3 da Licença
 *  ou (a seu critério) qualquer versão posterior.
 *
 *  Este programa é distribuído na esperança de que seja útil,
 *  mas SEM QUALQUER GARANTIA; sem a garantia implícita de
 *  COMERCIALIZAÇÃO OU ADEQUAÇÃO A UM DETERMINADO PROPÓSITO. Veja a
 *  Licença Pública Geral GNU para obter mais detalhes.
 *
 *  Você deve ter recebido uma cópia da Licença Pública Geral GNU
 *  junto com este programa. Se não, veja <https://www.gnu.org/licenses/>.
 *
 *  ENGLISH:
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 */

/**************************************************************************
 *   ____   _   _   ___   ____       _      ____     ___    _   _   _ 
 *  / ___| | | | | |_ _| |  _ \     / \    |  _ \   / _ \  | | | | | |
 * | |     | | | |  | |  | | | |   / _ \   | | | | | | | | | | | | | |
 * | |___  | |_| |  | |  | |_| |  / ___ \  | |_| | | |_| | |_| |_| |_|
 *  \____|  \___/  |___| |____/  /_/   \_\ |____/   \___/  (_) (_) (_)
 *
 * ISTO NÃO É UMA IMPLEMENTAÇÃO SEGURA!
 * ESTE PROGRAMA ***NÃO*** FOI FEITO PARA SER USADO EM SITUAÇÕES PRÁTICAS!
 *
 * Esta é uma implementação de uma cifra de Feistel, com o objetivo de
 * ilustrar como a arquitetura, vista em livros-texto, é traduzida em
 * um programa. No entanto, ela foi feita de maneira DESCUIDADA,
 * inclusive para que possa ser quebrada facilmente (porque pode ser
 * usada também como ferramenta didática no ensino de criptanálise).
 *
 * Deve estar claro que esta cifra NÃO funciona de maneira
 * segura. Mesmo com modos de operação seguros. Mesmo com vetores de
 * inicialização vindos de um gerador de alta qualidade. Mesmo que
 * < inserir aqui qualquer cuidado extra imaginável >.
 **************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <limits.h>
#include <stdint.h>
#include <unistd.h>

/*
Exemplos de uso:

I) Compilando

É um programa C que lê e escreve em arquivos; e que precisa de um
parâmetro de linha de comando.

gcc -Wall -o cipher cipher.c

produzirá o binário 'cipher'.

Construí e testei em Linux. Não faço idéia se funcionará em outros
sistemas.

II) Encriptando um arquivo

Por ser uma implementação feita com pressa e somente para mostrar como
a rede de Feistel é construída, o programa tem as seguintes limitações:

1. O texto claro deve ser SEMPRE múltiplo de oito (não é para usar na
   prática, mas para entender como funciona). Se quiser, faça padding
   antes de encriptar.
2. O tamanho do arquivo precisa ser passado na linha de comando (só pra
   não precisar fazer condições extra ao ler o arquivo). Novamente, não
   é para usar em situações práticas...
3. Os arquivos tem nomes FIXOS.
   clear  ->  texto claro
   enc    ->  texto cifrado (criado se não existir)
   key    ->  chave (somente 64 bits são lidos)

Por exemplo, see o arquivo tem 8000 bytes,

./cipher 8000

vai:
1. ler do arquivo 'key' a chave;
2. let do arquivo 'clear' 8000 bytes;
3. encriptar
4. gravar o resultado no arquivo 'enc'.

III) Experimento para ver o aumento da entropia

1. Gere 1000000 bytes aleatórios a partir de /dev/random
2. Codifique em base64, para ter uma entrada com baixa entropia
3. O arquivo ficará com 1350880 bytes
4. Use o programa para encriptar
5. use 'ent' (não é excelente, mas dá alguma medida e entropia),
   tanto em clear como em enc.

dd if=/dev/random of=clear.bin count=1 bs=1000000
base64 clear.bin > clear
./a.out 1350880
ent clear
ent enc

Sugestão: verificar a entropia com diferentes modos de operação. :)
*/


/******************
 *                *
 *   UTILIDADES   *
 *                *
 ******************/

/* Rotaciona um inteiro x (dos tipos uint32_t e uint64_t) n bits à
   esquerda.  Necessário porque (surpreendentemente) não há operador
   de rotação em C padrão. */
#define ROT32(x,n) ((x << n)|(x >> (32 - n)))
#define ROT64(x,n) ((x << n)|(x >> (64 - n)))

/* Função muito rudimentar para reportar erros... */
void error (char *msg) {
    fprintf(stderr,"%s\n",msg);
    exit(-1);
}

/*********************
 *                   *
 *      S-BOXES      *
 *                   *
 *********************/


/* S4_ são as S-boxes de 4 bits. As duas primeiras são invertíveis; as
  outras duas não.  Foram criadas despretensiosamente, digitando os
  números (e com certeza há problemas nelas, justamente por isso!)  */
uint8_t S4_1[] = { 0x8, 0xA, 0xB, 0x1, 0xF, 0x4, 0x7, 0xE, 0x5, 0xD, 0x0, 0x2, 0x3, 0x6, 0xC, 0x9 };
uint8_t S4_2[] = { 0xE, 0x7, 0x4, 0xF, 0x1, 0xB, 0xA, 0x8, 0x9, 0xC, 0x6, 0x3, 0x2, 0x0, 0xD, 0x5 };
uint8_t S4_3[] = { 0x2, 0xE, 0x1, 0x5, 0x9, 0xA, 0x4, 0x8, 0x8, 0x0, 0xD, 0x7, 0xC, 0x3, 0xF, 0x6 }; /* não invertível */
uint8_t S4_4[] = { 0xA, 0x5, 0xD, 0x2, 0x0, 0xF, 0x1, 0x7, 0xD, 0x6, 0x9, 0xE, 0x3, 0xB, 0x4, 0xC }; /* não invertível */

/* A seguir há funções que aplicam as S-boxes de 4 bytes em um byte. Sempre aplicamos uma
   invertível e uma não invertível, portanto o efeito é que estas funções são, todas,
   NÃO-invertíveis. */

/* s4_14 aplica as S-boxes 1 e 4 nas duas metades de um byte */
uint8_t S4_14(uint8_t x) {
    uint8_t first  = S4_1[(x & 0xf0) >> 4]; /* S4_1 na primeira parte de x */
    uint8_t second = S4_4[(x & 0x0f)];      /* S4_4 na segunda parte de x  */
    return second | (first << 4);
}

/* s4_23 aplica as S-boxes 2 e 3 nas duas metades de um byte */
uint8_t S4_23(uint8_t x) {
    uint8_t first  = S4_2[(x & 0xf0) >> 4]; /* S4_2 na primeira parte de x */
    uint8_t second = S4_3[(x & 0x0f)];      /* S4_3 na segunda parte de x  */
    return second | (first << 4);
}

/* s4_31 aplica as S-boxes 3 e 1 nas duas metades de um byte */
uint8_t S4_31(uint8_t x) {
    uint8_t first  = S4_3[(x & 0xf0) >> 4]; /* S4_3 na primeira parte de x */
    uint8_t second = S4_1[(x & 0x0f)];      /* S4_1 na segunda parte de x  */
    return second | (first << 4);
}

/* s4_42 aplica as S-boxes 4 e 2 nas duas metades de um byte */
uint8_t S4_42(uint8_t x) {
    uint8_t first  = S4_2[(x & 0xf0) >> 4]; /* S4_4 na primeira parte de x */
    uint8_t second = S4_3[(x & 0x0f)];      /* S4_2 na segunda parte de x  */
    return second | (first << 4);
}


/*****************
 *               *
 *   DEBUGGING   *
 *               *
 *****************/

void print_8(uint32_t x) {
    printf("[ ");
    for (int i=0; i<8; i++) {
        if (x & ((uint32_t)1 << 7)) /* Primeiro bit */
            printf("1 ");
        else
            printf("0 ");
        x = x << 1;
    }
    printf("]\n");           
}

void print_32(uint32_t x) {
    printf("[ ");
    for (int i=0; i<32; i++) {
        if (x & ((uint32_t)1 << 31)) /* Primeiro bit */
            printf("1 ");
        else
            printf("0 ");
        x = x << 1;
    }
    printf("]\n");           
}

void print_64(uint64_t x) {
    printf("[ ");
    for (int i=0; i<64; i++) {
        if (x & ((uint64_t)1 << 63)) /* Primeiro bit */
            printf("1 ");
        else
            printf("0 ");
        x = x << 1;
    }
    printf("]\n");           
}

/****************
 *              *
 *     CORE     *
 *              *
 ****************/

/* A função de rodada da cifra de Feistel.
   Nada especial: a metade do bloco que será operada é
   passada em IN e o número da rodada em ROUND.
   A função rotaciona os bits do bloco de forma que
   sua aplicação em diferentes rodadas terá efeito
   diferente (é interessante que o bloco é rotacionado
   em DEZ bits -- não múltiplo de oito, e portanto as
   S-boxes atuam em partes completamente diferentes
   do bloco, e de forma diferente, em cada rodada). */
uint32_t f(uint32_t in, uint32_t key, int round) {
    in = ROT32(in, 10);

    uint32_t out;

    /* Quebramos os 32 bits em quatro partes de 8 bits cada: */
    uint8_t Ain = (uint8_t) in & 0xff;
    uint8_t Bin = (uint8_t) ((in & 0xff00) >> 8);
    uint8_t Cin = (uint8_t) ((in & 0xff0000) >> 16);
    uint8_t Din = (uint8_t) ((in & 0xff000000) >> 24);

    /* E em cada byte, aplicamos pares de S-boxes (duas S-boxes
       de 4 bits atuam nos 8 bits do byte). */
    out = (uint32_t) S4_14(Ain)
        | (uint32_t) S4_23(Bin) << 8
        | (uint32_t) S4_42(Cin) << 16
        | (uint32_t) S4_31(Din) << 24;

    return key ^ out;
}

/* Permutação (para difusão) */
uint64_t P_box(uint64_t in) {
    uint64_t out = 0;
    uint64_t v;
    /*
       i passa pelos bytes da entrada, j pelos da saída.
       Cada byte da entrada terá seus bits distribuídos nos da saída.
       Por exemplo, no QUARTO byte da entrada, o TERCEIRO bit:
       será enviado para o QUARTO **BIT** do *TERCEIRO **BYTE** da
       saída.
               3                  4                  5         ... <= bytes
                          +----------------+
                          | . . . B . . . .|
                          +----------------+
                                  |
                   ______________/
                  /
                 |
                 v
       +----------------+
       | . . . . B . . .|
       +----------------+

       Portanto, passamos um índice i, j de zero a oito.
       i é usado nos BYTES de entrada e BITS de saída
       j é usado nos BITS de entrada e BYTES de saída.
     */
    for (int i = 0; i < 8; i++)
        for(int j=0; j < 8; j++) {
            v = (in >> (8*i + j)) & 0x1;   /* i-ésimo byte, j-ésimo bit */
            out |= v << (8*j + i); /* j-ésimo byte, i-ésimo bit */
        }
    return out;
}

/* Uma rodada da cifra. Separa as duas metades do bloco,
   e aplica a função de rodada à maneira das cifras de Feistel.
   Ao final, faz ou-exclusivo com a chave de rodada.*/
uint64_t feistel_round(uint64_t input, uint64_t key, int round) {
    uint32_t Lin = (input >> 32); 
    uint32_t Rin = (input & 0xffffffff);
    uint64_t Rout = 0;
    uint64_t Lout = 0;
    Lout = Rin;
    Rout = Lin ^ f(Rin, key, round);
    return P_box((Rout | (Lout << 32)));
}

/* Escalonamento de chaves.
   A chave é rotacionada e passada pelas S-boxes a cada rodada. */
uint32_t round_key(uint64_t k, int round) {
    k = ROT64(k, (round * 5)); /* Não múltiplo de 8. As S-boxes serão
                                  aplicadas a bytes completamente
                                  diferentes na chave em rodadas
                                  diferentes! */

    /* Dividimos a chave em bytes */
    uint8_t Ak = (uint8_t) k & 0xff;
    uint8_t Bk = (uint8_t) ((k & 0xff00) >> 8);
    uint8_t Ck = (uint8_t) ((k & 0xff0000) >> 16);
    uint8_t Dk = (uint8_t) ((k & 0xff000000) >> 24);
    uint8_t Ek = (uint8_t) ((k & 0xff00000000) >> 32);
    uint8_t Fk = (uint8_t) ((k & 0xff0000000000) >> 40);
    uint8_t Gk = (uint8_t) ((k & 0xff000000000000) >> 48);
    uint8_t Hk = (uint8_t) ((k & 0xff00000000000000) >> 56);

    /* Aplicamos as S-boxes nos bytes, e "concatenamos" (usando ou e shifts)
       o resultado. */
    uint64_t largekey = (uint32_t) S4_14(Hk)
        | (uint64_t) S4_23(Gk) << 8
        | (uint64_t) S4_42(Fk) << 16
        | (uint64_t) S4_31(Ek) << 24
        | (uint64_t) S4_14(Ak) << 32
        | (uint64_t) S4_23(Bk) << 40
        | (uint64_t) S4_42(Ck) << 48
        | (uint64_t) S4_31(Dk) << 50;

    uint32_t outkey;
    if (round%2) outkey = (uint32_t) (largekey & 0xffffffff); /* 4 últimos bytes */
    else         outkey = (uint32_t) ((largekey >> 32) & 0xffffffff); /* 4 primeiros bytes */
    return  outkey;
}

/* Encripta um bloco.
   O bloco passa por doze rodadas da cifra. */
uint64_t encrypt_block(uint64_t block, uint64_t key) {
    for (int r=0; r<12; r++)
        block = feistel_round(block, round_key(key, r), r);
    return block;
}


/********************************************
 *                                          *
 *       OPERAÇÃO EM MÚLTIPLOS BLOCOS       *
 *                                          *
 ********************************************/

/* Encripta no modo ECB */
uint8_t *encrypt_ecb(uint8_t *m, uint64_t key, size_t size) {
    if (size % 8) error("Faça padding antes! A mensagem deve ter um número de bytes múltiplo de 8.");

    uint8_t *out = malloc(size);
    if (NULL == out) error("Não consegui alocar memória para o texto cifrado!");

    int i=0;
    for (int b=0; b < size; b+=8, i++) {
        ((uint64_t *)out)[i] = encrypt_block(((uint64_t *)m)[i], key);
    }
    return out;
}

/* Encripta no modo CBC */
uint8_t *encrypt_cbc(uint8_t *m, uint64_t iv, uint64_t key, size_t size) {
    if (size % 8) error("Faça padding antes! A mensagem deve ter um número de bytes múltiplo de 8.");

    uint8_t *out = malloc(size);
    if (NULL == out) error("Não consegui alocar memória para o texto cifrado!");

    int i=0;
    for (int b=0; b < size; b+=8, i++) {
        ((uint64_t *)out)[i] = encrypt_block(iv ^ (((uint64_t *)m)[i]), key);
        iv = ((uint64_t *)out)[i];
    }
    return out;
}

/***************************************
 *                                     *
 *       OPERAÇÕES EM ARQUIVOS         *
 *                                     *
 ***************************************/

/* Aceita file descriptor e lê a chave (64 bits). */
uint64_t read_key(int fd) {
    uint64_t k;
    ssize_t c = read(fd, &k, 8);
    if (c!= 8) error("Could not read key!");
    return k;
}

/* Aceita file descriptor e tamanho (em bytes), e lê o texto claro.
   Precisa do tamanho para simplificar o código (o tamanho da mensagem
   é passado na linha de comando -- não é um programa para uso
   prático...) */
uint8_t *read_clear(int in, size_t size) {
    uint8_t *buf = malloc(size);
    ssize_t c = read(in, buf, size);
    if (c != size) error("Could not read clear text.");
    return buf;
}

/* Escreve o texto encriptado. Recebe um file descriptor (encfile), o
   buffer e o tamanho em bytes. */
void write_enc(int encfile, uint8_t *buf, size_t size) {
    ssize_t c = write(encfile, buf, size);
    if (c != size) error("Could not write encrypted file.");
}


/**************
 *            *
 *    MAIN    *
 *            *
 **************/

int main(int argc, char *argv[]) {
    if (argc != 2) error ("One argument, please (the size)");
    size_t size = atoi(argv[1]);
    int keyfile;
    int clearfile;
    int encfile;
    keyfile = open("key", O_RDONLY);
    clearfile = open("clear", O_RDONLY);
    encfile = open("enc", O_WRONLY|O_CREAT,S_IRUSR|S_IWUSR);

    uint64_t iv = 64232476761111; /* "Garantidamente" aleatório! :) */
    
    uint64_t key = read_key(keyfile);
    uint8_t *text = read_clear(clearfile, size);

    /* As duas próximas linhas encriptam em ECB e em CBC. Só
       uma delas deve ficar descomentada (obviamente); */
    uint8_t *enc = encrypt_ecb(text, key, size);    /* ECB */
    //uint8_t *enc = encrypt_cbc(text, iv, key, size);  /* CBC */
    
    write_enc(encfile, enc, size);
}
